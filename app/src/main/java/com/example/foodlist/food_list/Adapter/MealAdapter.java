package com.example.foodlist.food_list.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.foodlist.food_list.Model.Meal;
import com.example.foodlist.food_list.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MealAdapter extends ArrayAdapter<Meal> {
    List<Meal> mealList;
    Context context;
    int resource;

    public MealAdapter(@NonNull Context context, int resource, @NonNull List<Meal> mealList){
        super(context, resource, mealList);
        this.context=context;
        this.resource=resource;
        this.mealList=mealList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        if (convertView==null){
            LayoutInflater layoutInflater=(LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.item_grid,null,true);
        }
        Meal mealModel=getItem(position);
        ImageView img=(ImageView)convertView.findViewById(R.id.imView);
        Picasso.with(context)
                .load(mealModel.getStrMealThumb())
                .into(img);
        return convertView;
    }
}
