package com.example.foodlist.food_list.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    private static Retrofit retrofit = null;
    public static String BASE_URL = "http://www.themealdb.com/api/";
    Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    public static Retrofit getApiClient(){
        if (retrofit == null ){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
