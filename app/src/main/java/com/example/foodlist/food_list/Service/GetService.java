package com.example.foodlist.food_list.Service;

import com.example.foodlist.food_list.Model.MealModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetService {
    @GET("json/v1/1/filter.php")
    Call<MealModel> getMeal(@Query("c") String parameter);
}