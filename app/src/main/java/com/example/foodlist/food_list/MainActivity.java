package com.example.foodlist.food_list;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.foodlist.food_list.Adapter.MealAdapter;
import com.example.foodlist.food_list.Adapter.ViewPagerAdapter;
import com.example.foodlist.food_list.Model.Meal;
import com.example.foodlist.food_list.Model.MealModel;
import com.example.foodlist.food_list.Service.Api;
import com.example.foodlist.food_list.Service.GetService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    GridView grid;
    List<Meal> mealList;
    RecyclerView.LayoutManager layoutManager;
    MealAdapter mealAdapter = null;
    String path;
    String parameter;
    private Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);

        viewPager.setAdapter(viewPagerAdapter);
        grid = (GridView) findViewById(R.id.gridview);
        mealList = new ArrayList<>();
        path = getIntent().getStringExtra("path");
        tampilMeal();

        // event klik
//        grid.setOnItemClickListener(new AdapterView.OnItemClickListener()
//        {
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
//                                    long arg3) {
//                Toast.makeText(MainActivity.this, mealAdapter.getItem(position), Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    private void tampilMeal() {
//        api = (Api) Api.getApiClient().create(GetService.class);
//        Call<MealModel> call = ((GetService) api).getMeal();
        GetService service = Api.getApiClient().create(GetService.class);
        Call<MealModel> call = service.getMeal(parameter);
        call.enqueue(new Callback<MealModel>() {
            @Override
            public void onResponse(Call<MealModel> call, Response<MealModel> response) {
//                mealList = (List<Meal>) response.body();
//                mealAdapter = new MealAdapter(getApplicationContext(), R.layout.item_grid, mealList);
//                grid.setAdapter(mealAdapter);
                if (response.code() == 200){
                    mealList = response.body().getMeals();
                    mealAdapter = new MealAdapter(getApplicationContext(), R.layout.item_grid, mealList);
                    grid.setAdapter(mealAdapter);
                    Toast.makeText(getApplicationContext(), (CharSequence) response.body().getMeals(), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), response.code() + " : " +response.message(), Toast.LENGTH_SHORT).show();
                }
                }
            @Override
            public void onFailure(Call<MealModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}